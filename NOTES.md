
# project specification:
https://hub.animorph.coop/f/141047

# approach outline
for each frame of video (opencv VideoCapture)
 - apply dlib 68-point face detection and landmark finding (get_frontal_face_detector() and shape_predictor(<path to 68-point model>))
 - use some of those points to find loose bounding boxes around each eye
 - apply the approach of https://github.com/RithikBanerjee/iris-detector/blob/master/Documentation/Localization%20%26%20Segmentation%20of%20Non-ideal%20iris%20images.pdf
